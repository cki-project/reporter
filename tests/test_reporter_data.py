"""Test data.py classes and methods."""

import contextlib
from importlib import resources
import json
from pathlib import Path
import tempfile
from unittest import TestCase
from unittest import mock
from unittest.mock import patch

from cki_lib.kcidb import KCIDBFile
from cki_lib.kcidb.checks import broken_boot_tests
import datawarehouse.objects
import responses

from reporter.data import CheckoutData
from reporter.data import TestData
from reporter.settings import LOGGER

from . import assets
from . import mocks

ASSETS = resources.files(assets)

BUILDS = [
    {
        "id": 0,
        "misc": {"iid": 0},
    },
    {
        "id": 1,
        "misc": {"iid": 1},
    },
]

CHECKOUT = {"id": "redhat:123", "misc": {"iid": 0}}
ISSUES = []
TESTS = []

KCIDBFILE = {
    "builds": BUILDS,
    "checkouts": [CHECKOUT],
    "issueoccurrences": ISSUES,
    "testresults": [],
    "tests": TESTS,
}


class TestRequests(TestCase):
    """Tests for the API requests done by reporter."""

    @responses.activate
    def test_build_list(self):
        """Test the listing of builds."""
        responses.get(
            f'http://localhost/api/1/kcidb/checkouts/{CHECKOUT["id"]}/all', json=KCIDBFILE
        )
        responses.get("http://localhost/api/1/kcidb/checkouts/0/all", json=KCIDBFILE)
        responses.get("http://localhost/api/1/kcidb/checkouts/0", json=CHECKOUT)

        checkout = CheckoutData(0)
        builds = checkout.build_data.builds
        self.assertEqual(2, len(builds))
        self.assertEqual([BUILDS[0]["id"], BUILDS[1]["id"]], [build.id for build in builds])

        # Make sure the builds iterator is reusable
        self.assertEqual([BUILDS[0]["id"], BUILDS[1]["id"]], [build.id for build in builds])


class TestReporterData(TestCase):
    """Tests for reporter/data.py ."""

    @classmethod
    @contextlib.contextmanager
    def dump_kcidb_json(cls, kcidb_data):
        """Context manager that writes the given data into a file and removes it on exit."""
        tmpdir = tempfile.TemporaryDirectory()
        path = Path(tmpdir.name, "kcidb_data.json")
        path.write_text(json.dumps(kcidb_data), encoding="utf-8")
        try:
            yield path
        finally:
            tmpdir.cleanup()

    @responses.activate
    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data(self, checkouts_get_mock):
        """Test the CheckoutData class and it's properties."""
        checkout = mocks.get_mocked_checkout(valid=True, log_url='https://host/file')
        build = mocks.get_mocked_build(checkout_id=checkout.id, valid=True)
        test = mocks.get_mocked_test(build_id=build.id, status="PASS", waived=False)
        occurrence = mocks.get_mocked_issue_occurrence(checkout_id=checkout.id)
        kcidbfile = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=[build], tests=[test], issueoccurrences=[occurrence]
        )
        checkouts_get_mock.return_value.all.get.return_value = kcidbfile

        # actual call
        checkout_data = CheckoutData(checkout.id)

        checkouts_get_mock.assert_called_once_with(id=checkout.id)

        self.assertEqual(checkout_data.checkout, checkout)

        responses.get('https://host/file', body=b'bar')

        self.assertEqual(checkout_data.mergelog, 'bar')
        self.assertEqual(checkout_data.issues_occurrences, [occurrence])

        self.assertEqual(checkout_data.build_data.builds, [build])
        self.assertEqual(checkout_data.test_data.tests, [test])

        self.assertTrue(checkout_data.result)

    def test_checkout_data_from_file(self):
        """
        Test that CheckoutData instance from file is created as expected.

        Only a basic check is done on select fields.
        This verifies processing of the kcidb_all file.
        """
        test_file_path = ASSETS / "kcidb_all_checkout_127491_aarch64_reduced_and_triaged.json"
        kcidb_file = KCIDBFile(test_file_path, validate=False)
        checkout_data = CheckoutData("redhat:1163293372", kcidb_file)
        self.assertFalse(checkout_data.result)
        self.assertFalse(checkout_data.summary)

        with self.subTest("Ensure testresults are present"):
            self.assertEqual(len(checkout_data.all_data.testresults), 90)
            for testresult in checkout_data.all_data.testresults:
                self.assertIsInstance(testresult, datawarehouse.objects.KCIDBTestResult)

        with self.subTest("Ensure builds are present"):
            self.assertEqual(len(checkout_data.all_data.builds), 2)
            for build in checkout_data.all_data.builds:
                self.assertIsInstance(build, datawarehouse.objects.KCIDBBuild)

            expected_build_ids = [
                'redhat:1163293372-aarch64-kernel',
                'redhat:1163293372-x86_64-kernel'
            ]
            actual_build_ids = [build.id for build in checkout_data.all_data.builds]
            self.assertCountEqual(actual_build_ids, expected_build_ids)

        with self.subTest("Ensure checkouts are present"):
            self.assertEqual(len(checkout_data.all_data.checkouts), 1)
            self.assertIsInstance(checkout_data.all_data.checkouts[0],
                                  datawarehouse.objects.KCIDBCheckout)
            self.assertEqual(checkout_data.all_data.checkouts[0].id, 'redhat:1163293372')

        with self.subTest("Ensure the expected failed test is present"):
            self.assertEqual(len(checkout_data.test_data.failed_tests), 1)
            failed_test = checkout_data.test_data.failed_tests[0]
            self.assertEqual(failed_test.id, "redhat:1163293372-aarch64-kernel_upt_41")

            with self.subTest("Ensure there are expected nonpassing testresults"):
                expected_nonpassing_testresult_ids = [
                    "redhat:1163293372-aarch64-kernel_upt_41.1707005666",
                    "redhat:1163293372-aarch64-kernel_upt_41.1707005668",
                ]
                actual_testresult_ids = [
                    testresult.id for testresult in failed_test.nonpassing_ran_testresults
                ]
                self.assertCountEqual(actual_testresult_ids, expected_nonpassing_testresult_ids)

            with self.subTest("Ensure there are expected untriaged testresults"):
                expected_nonpassing_testresult_ids = [
                    "redhat:1163293372-aarch64-kernel_upt_41.1707005668",
                ]
                actual_testresult_ids = [
                    testresult.id for testresult in failed_test.untriaged_testresults
                ]
                self.assertCountEqual(actual_testresult_ids, expected_nonpassing_testresult_ids)

            with self.subTest("Ensure untriaged tests are present"):
                expected_untriaged_test_ids = [
                    "redhat:1163293372-aarch64-kernel_upt_7",
                    "redhat:1163293372-aarch64-kernel_upt_19",
                    "redhat:1163293372-aarch64-kernel_upt_41",
                ]
                actual_untriaged_test_ids = [t.id for t in checkout_data.test_data.untriaged_tests]
                self.assertCountEqual(actual_untriaged_test_ids, expected_untriaged_test_ids)

            with self.subTest("Ensure untriaged failed tests are present"):
                expected_untriaged_failed_test_ids = ["redhat:1163293372-aarch64-kernel_upt_41"]
                actual_untriaged_test_ids = [
                    t.id for t in checkout_data.test_data.untriaged_failed_tests
                ]
                self.assertCountEqual(actual_untriaged_test_ids, expected_untriaged_failed_test_ids)

            with self.subTest("Ensure triaged tests are present"):
                expected_triaged_test_ids = ["redhat:1163293372-aarch64-kernel_upt_40"]
                actual_triaged_test_ids = [t.id for t in checkout_data.test_data.triaged_tests]
                self.assertCountEqual(actual_triaged_test_ids, expected_triaged_test_ids)

            with self.subTest("Ensure there is one expected missed test"):
                expected_missed_tests_ids = ['redhat:1163293372-aarch64-kernel_upt_19']
                actual_missed_test_ids = [missed_test.id for missed_test in
                                          checkout_data.test_data.missed_tests]
                self.assertCountEqual(actual_missed_test_ids, expected_missed_tests_ids)

            with self.subTest("Ensure there is one expected passed test"):
                expected_passed_test_ids = ['redhat:1163293372-aarch64-kernel_upt_39']
                actual_passed_test_ids = [passed_test.id for passed_test in
                                          checkout_data.test_data.passed_tests]
                self.assertCountEqual(actual_passed_test_ids, expected_passed_test_ids)

            with self.subTest("Ensure there is one waived test"):
                expected_waived_test_ids = ['redhat:1163293372-aarch64-kernel_upt_40']
                actual_waived_test_ids = [waived_test.id for waived_test in
                                          checkout_data.test_data.waived_tests]
                self.assertCountEqual(actual_waived_test_ids, expected_waived_test_ids)

            with self.subTest("Ensure ValueError is raised, when checkout_id does not match"):
                with self.assertRaises(ValueError):
                    CheckoutData("nonmatching", KCIDBFile(test_file_path, validate=False))

        with self.subTest("Ensure ValueError is raised, when fail to parse the KCIDB file"):
            invalid_kcidb = KCIDBFILE | {"issueoccurrences": 1}  # 1 is not a list
            with (
                self.dump_kcidb_json(invalid_kcidb) as path,
                self.assertRaisesRegex(
                    ValueError, r"Failed to parse data into dw-api-lib instance"
                ),
            ):
                CheckoutData(CHECKOUT["id"], kcidb_file=KCIDBFile(path, validate=False))

    @patch("reporter.data.KCIDBFile")
    def test_checkout_data_warns_about_deprecated_init_argument(self, mocked_kcidb_file):
        """Assert we are warning about the new interface, but it still works as before."""
        checkout_id = "redhat:1163293372"
        mocked_kcidb_file.return_value.checkouts = [mock.Mock(id=checkout_id)]

        test_file_path = ASSETS / "kcidb_all_checkout_127491_aarch64_reduced_and_triaged.json"
        kcidb_file = KCIDBFile(test_file_path, validate=False)
        checkout_data = CheckoutData(checkout_id, kcidb_file)

        with self.assertWarns(DeprecationWarning):
            deprecated = CheckoutData(checkout_id, test_file_path)

        self.assertTrue(
            checkout_data.all_data == deprecated.all_data == mocked_kcidb_file.return_value,
            "Smoke test that we're using the mocked dw_api_lib.KCIDBFile.",
        )
        self.assertEqual(mocked_kcidb_file.call_count, 2, "Expected one good and on bad call")
        self.assertEqual(*mocked_kcidb_file.call_args_list, "Expected all calls to be equal")

    @patch("reporter.utils.add_testresults_to_kcidbfile_dict")
    @patch('reporter.data.KCIDBFile')
    def test_checkout_data_from_file_invalid_checkout_id(
        self, kcidbfile_mock, add_testresults_mock
    ):
        """This test checks the input validation performed by CheckoutData.__init__()."""
        checkout = mocks.get_mocked_checkout(id="")
        kcidbfile_mock.return_value.checkouts = [checkout]
        with self.assertRaises(TypeError):
            CheckoutData(None, kcidb_file=mock.Mock())

        with self.assertRaises(ValueError):
            CheckoutData("", kcidb_file=mock.Mock())

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_failed_build_result(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        from reporter.data import CheckoutData

        checkout = mocks.get_mocked_checkout(valid=True)
        builds = [
            mocks.get_mocked_build(checkout_id=checkout.id, valid=True),
            mocks.get_mocked_build(checkout_id=checkout.id, valid=False),  # This is enough to fail
        ]
        kci_file = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=builds, tests=[], issueoccurrences=[]
        )
        checkouts_get_mock.return_value.all.get.return_value = kci_file

        checkout_data = CheckoutData('1234')

        self.assertFalse(checkout_data.result)
        self.assertFalse(checkout_data.summary)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_known_issue_build_result(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        from reporter.data import CheckoutData

        checkout = mocks.get_mocked_checkout(valid=True)
        builds = [
            mocks.get_mocked_build(checkout_id=checkout.id, valid=True),
            mocks.get_mocked_build(checkout_id=checkout.id, valid=False, id="triaged-build"),
        ]
        occurrence = mocks.get_mocked_issue_occurrence(build_id="triaged-build")
        kci_file = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=builds, tests=[], issueoccurrences=[occurrence]
        )
        checkouts_get_mock.return_value.all.get.return_value = kci_file

        checkout_data = CheckoutData('1234')

        self.assertTrue(checkout_data.result)
        self.assertTrue(checkout_data.summary)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_summary(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        from reporter.data import CheckoutData

        checkout = mocks.get_mocked_checkout(valid=True)
        build = mocks.get_mocked_build(checkout_id=checkout.id, valid=True)
        test = mocks.get_mocked_test(build_id=build.id, status="MISS")
        kcidbfile = mocks.get_mocked_kcidbfile(checkouts=[checkout], builds=[build], tests=[test])
        checkouts_get_mock.return_value.all.get.return_value = kcidbfile

        checkout_data = CheckoutData('1234')

        self.assertTrue(checkout_data.result)
        self.assertIsNone(checkout_data.test_data.summary, "All tests missed, should return None")
        self.assertIsNone(checkout_data.summary, "All fine, should return test_data.summary")

    @patch("reporter.data.broken_boot_tests", wraps=broken_boot_tests)
    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_filter_non_booting_tests(self, checkouts_get_mock,
                                                    broken_boot_tests_mock):
        """Test the CheckoutData filter_non_booting_tests method."""
        from reporter.data import CheckoutData

        checkout = mocks.get_mocked_checkout(valid=True)
        build = mocks.get_mocked_build(checkout_id=checkout.id, valid=True)
        boot_test = mocks.get_mocked_test(
            build_id=build.id, id="boot-test", comment="Boot test", status="MISS"
        )
        other_test = mocks.get_mocked_test(build_id=build.id, id="other-test", status="PASS")
        all_data_mock = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=[build], tests=[boot_test, other_test], issueoccurrences=[]
        )
        checkout.all.get.return_value = all_data_mock
        checkouts_get_mock.return_value = checkout

        with self.assertLogs(logger=LOGGER, level="DEBUG") as log_ctx:
            checkout_data = CheckoutData("1234")

        expected_log = f"DEBUG:{LOGGER.name}:Builds that failed to boot: " + str({build.id})
        self.assertIn(expected_log, log_ctx.output)

        broken_boot_tests_mock.assert_called_once_with(dw_all=all_data_mock)

        self.assertFalse(checkout_data.all_data.builds[0].boots)
        self.assertEqual(checkout_data.all_data.tests, [boot_test])

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_filter_builds_by_id(self, checkouts_get_mock):
        """Test the CheckoutData filters builds and their descendants when given a build_id."""
        from reporter.data import CheckoutData

        checkout = mocks.get_mocked_checkout(valid=True)
        asdf_build = mocks.get_mocked_build(checkout_id=checkout.id, valid=True, id="asdf123")
        qwer_build = mocks.get_mocked_build(checkout_id=checkout.id, valid=True, id="qwer456")
        test1 = mocks.get_mocked_test(build_id=asdf_build.id, status="PASS")
        test2 = mocks.get_mocked_test(build_id=qwer_build.id, status="MISS")
        test1_result1 = mocks.get_mocked_test_results(test_id=test1.id, status="PASS")
        test1_result2 = mocks.get_mocked_test_results(test_id=test1.id, status="MISS")
        test2_result1 = mocks.get_mocked_test_results(test_id=test2.id, status="ERROR")
        test2_result2 = mocks.get_mocked_test_results(test_id=test2.id, status="FAIL")
        occurrence1 = mocks.get_mocked_issue_occurrence(testresult_id=test1_result1.id)
        occurrence2 = mocks.get_mocked_issue_occurrence(testresult_id=test1_result2.id)
        occurrence3 = mocks.get_mocked_issue_occurrence(testresult_id=test2_result1.id)
        occurrence4 = mocks.get_mocked_issue_occurrence(testresult_id=test2_result2.id)

        checkout.all.get.return_value = mocks.get_mocked_kcidbfile(
            checkouts=[checkout],
            builds=[asdf_build, qwer_build],
            tests=[test1, test2],
            results=[test1_result1, test1_result2, test2_result1, test2_result2],
            issueoccurrences=[occurrence1, occurrence2, occurrence3, occurrence4],
        )
        checkouts_get_mock.return_value = checkout

        with self.subTest(build_id="asdf123"):
            checkout_data = CheckoutData(checkout.id, build_id="asdf123")

            self.assertEqual(checkout_data.all_data.checkouts, [checkout])
            self.assertEqual(checkout_data.all_data.builds, [asdf_build])
            self.assertEqual(checkout_data.all_data.tests, [test1])
            self.assertEqual(checkout_data.all_data.testresults, [test1_result1, test1_result2])
            self.assertEqual(checkout_data.all_data.issueoccurrences, [occurrence1, occurrence2])

        with self.subTest(build_id="nomatch"):
            with self.assertLogs(logger=LOGGER, level="WARNING") as log_ctx:
                checkout_data = CheckoutData(checkout.id, build_id="nomatch")

            self.assertIn(
                f"WARNING:{LOGGER.name}:No build matching the given ID: nomatch",
                log_ctx.output,
            )

            self.assertEqual(checkout_data.all_data.checkouts, [])
            self.assertEqual(checkout_data.all_data.builds, [])
            self.assertEqual(checkout_data.all_data.tests, [])
            self.assertEqual(checkout_data.all_data.testresults, [])
            self.assertEqual(checkout_data.all_data.issueoccurrences, [])

    @patch("reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get")
    def test_build_data(self, checkouts_get_mock):
        """Test the BuildData class and it's properties."""
        from reporter.data import CheckoutData

        checkout = mocks.get_mocked_checkout(valid=True)
        passed_build = mocks.get_mocked_build(checkout_id=checkout.id, valid=True, id="passed_id")
        failed_build = mocks.get_mocked_build(checkout_id=checkout.id, valid=False, id="failed_id")
        known_failure_build = mocks.get_mocked_build(
            checkout_id=checkout.id, valid=False, id="triaged_id"
        )
        issue = mocks.get_mocked_issue_occurrence(build_id="triaged_id")
        kcidbfile = mocks.get_mocked_kcidbfile(
            checkouts=[checkout],
            builds=[passed_build, failed_build, known_failure_build],
            issueoccurrences=[issue],
        )

        checkouts_get_mock.return_value.all.get.return_value = kcidbfile
        build_data = CheckoutData(checkout.id).build_data
        checkouts_get_mock.assert_called_once_with(id=checkout.id)

        self.assertEqual(build_data.passed_builds, [passed_build])
        self.assertEqual(build_data.failed_builds, [failed_build, known_failure_build])
        self.assertEqual(build_data.known_issues_builds, [known_failure_build])
        self.assertEqual(build_data.unknown_issues_builds, [failed_build])

    def test_build_data_get_kernel_variant(self):
        """Test the BuildData get_kernel_variant static method."""
        from reporter.data import BuildData

        checkout = mocks.get_mocked_checkout(valid=True)
        build_default = mocks.get_mocked_build(
            checkout_id=checkout.id, misc={"debug": False, "package_name": "kernel"}
        )
        build_debug = mocks.get_mocked_build(
            checkout_id=checkout.id, misc={"debug": True, "package_name": "kernel"}
        )
        build_64k = mocks.get_mocked_build(
            checkout_id=checkout.id, misc={"debug": False, "package_name": "kernel-64k"}
        )
        build_64k_debug = mocks.get_mocked_build(
            checkout_id=checkout.id, misc={"debug": True, "package_name": "kernel-64k"}
        )
        build_64k_debug_package = mocks.get_mocked_build(
            checkout_id=checkout.id, misc={"debug": False, "package_name": "kernel-64k-debug"}
        )
        build_64k_debug_both = mocks.get_mocked_build(
            checkout_id=checkout.id, misc={"debug": True, "package_name": "kernel-64k-debug"}
        )

        self.assertEqual(BuildData.get_kernel_variant(build_default), '')
        self.assertEqual(BuildData.get_kernel_variant(build_debug), 'debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k), '64k')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug), '64k-debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug_package), '64k-debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug_both), '64k-debug')
        # Removes given source_package_name prefix
        self.assertEqual(BuildData.get_kernel_variant(build_64k, 'kernel-64k'), '')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug, 'kernel-64k'), 'debug')

    @patch("reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get")
    def test_test_data(self, checkouts_get_mock):
        """Test the TestData class and it's properties."""
        from reporter.data import CheckoutData

        checkout = mocks.get_mocked_checkout(valid=True)
        build = mocks.get_mocked_build(checkout_id=checkout.id)

        pass_test = mocks.get_mocked_test(build_id=build.id, status="PASS", waived=False)
        pass_waived_test = mocks.get_mocked_test(build_id=build.id, status="PASS", waived=True)
        fail_test = mocks.get_mocked_test(build_id=build.id, status="FAIL", waived=False)
        fail_waived_test = mocks.get_mocked_test(build_id=build.id, status="FAIL", waived=True)
        error_test = mocks.get_mocked_test(build_id=build.id, status="ERROR", waived=False)
        miss_test = mocks.get_mocked_test(build_id=build.id, status="MISS", waived=False)
        none_test = mocks.get_mocked_test(build_id=build.id, status=None, waived=False)

        # Test without subtests
        fail_triaged_test1 = mocks.get_mocked_test(
            build_id=build.id, status="FAIL", waived=False, id="test_1"
        )
        # Test with ALL subtests triaged
        fail_triaged_test2 = mocks.get_mocked_test(
            build_id=build.id, status="FAIL", waived=False, id="test_2"
        )
        fail_triaged_test2_result1 = mocks.get_mocked_test_results(
            test_id=fail_triaged_test2.id, status="FAIL", id="test2_1"
        )
        # Test with all FAILED subtests triaged
        fail_triaged_test3 = mocks.get_mocked_test(
            build_id=build.id, status="FAIL", waived=False, id="test_3"
        )
        fail_triaged_test3_result1 = mocks.get_mocked_test_results(
            test_id=fail_triaged_test3.id, status="FAIL", id="test3_1"
        )
        fail_triaged_test3_result2 = mocks.get_mocked_test_results(
            test_id=fail_triaged_test3.id, status="ERROR", id="test3_2"
        )
        regression_test = mocks.get_mocked_test(
            build_id=build.id, status="FAIL", waived=False, id="test_4"
        )
        known_issue_occurrence1 = mocks.get_mocked_issue_occurrence(
            issue_attributes={"description": "failed", "resolved": False, "ticket_url": "url"},
            test_id=fail_triaged_test1.id,
        )
        known_issue_occurrence2 = mocks.get_mocked_issue_occurrence(
            issue_attributes={"description": "failed", "resolved": False, "ticket_url": "url"},
            testresult_id=fail_triaged_test2_result1.id,
        )
        known_issue_occurrence3 = mocks.get_mocked_issue_occurrence(
            issue_attributes={"description": "failed", "resolved": False, "ticket_url": "url"},
            testresult_id=fail_triaged_test3_result1.id,
        )
        regression_issue = mocks.get_mocked_issue_occurrence(
            is_regression=True,
            issue_attributes={"description": "failed", "resolved": True},
            test_id=regression_test.id,
        )
        tests = [
            pass_test,
            fail_test,
            pass_waived_test,
            fail_waived_test,
            error_test,
            miss_test,
            none_test,
            fail_triaged_test1,
            fail_triaged_test2,
            fail_triaged_test3,
            regression_test,
        ]

        occurs = [
            known_issue_occurrence1,
            known_issue_occurrence2,
            known_issue_occurrence3,
            regression_issue,
        ]

        kcidbfile = mocks.get_mocked_kcidbfile(
            checkouts=[checkout],
            builds=[build],
            tests=tests,
            results=[
                fail_triaged_test2_result1,
                fail_triaged_test3_result1,
                fail_triaged_test3_result2,
            ],
            issueoccurrences=occurs,
        )
        checkouts_get_mock.return_value.all.get.return_value = kcidbfile
        test_data = CheckoutData(checkout.id).test_data
        checkouts_get_mock.assert_called_once_with(id=checkout.id)

        self.assertEqual(test_data.tests, tests)

        self.assertEqual(
            test_data.failed_tests,
            [
                fail_test,
                fail_triaged_test1,
                fail_triaged_test2,
                fail_triaged_test3,
                regression_test,
            ],
        )
        self.assertEqual(test_data.passed_tests, [pass_test, pass_waived_test])
        self.assertEqual(test_data.missed_tests, [miss_test, none_test])
        self.assertEqual(test_data.nonmissed_tests,
                         [test for test in tests
                          if test not in test_data.missed_tests])
        self.assertEqual(test_data.errored_tests, [error_test])
        self.assertEqual(test_data.waived_tests, [pass_waived_test, fail_waived_test])
        self.assertEqual(test_data.triaged_tests, [fail_triaged_test1, fail_triaged_test2])
        self.assertEqual(
            test_data.untriaged_failed_tests, [fail_test]
        )  # I'm not expecting the waived test here
        self.assertEqual(test_data.regressions, [regression_test])
        known_issue = fail_triaged_test1.issues_occurrences[0].issue
        self.assertEqual(
            test_data.known_issues,
            {
                known_issue["ticket_url"]: {
                    "issue": known_issue,
                    "tests": [fail_triaged_test1, fail_triaged_test2],
                }
            },
        )

    @patch("reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get")
    def test_test_data_all_missed_check(self, checkouts_get_mock):
        """Test the TestData's all_missed_check method."""
        from reporter.data import CheckoutData

        checkout = mocks.get_mocked_checkout(valid=True)
        build = mocks.get_mocked_build(checkout_id=checkout.id, valid=True)
        pass_test = mocks.get_mocked_test(build_id=build.id, status="PASS", waived=False)
        miss_test1 = mocks.get_mocked_test(build_id=build.id, status="MISS", waived=False)
        miss_test2 = mocks.get_mocked_test(build_id=build.id, status="MISS", waived=False)

        kcidbfile_missed = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=[build], tests=[miss_test1]
        )
        kcidbfile_nonmissed = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=[build], tests=[miss_test2, pass_test]
        )

        checkouts_get_mock.return_value.all.get.return_value = kcidbfile_missed
        test_data_missed = CheckoutData(checkout.id).test_data
        checkouts_get_mock.assert_called_with(id=checkout.id)

        checkouts_get_mock.return_value.all.get.return_value = kcidbfile_nonmissed
        test_data_nonmissed = CheckoutData(checkout.id).test_data
        checkouts_get_mock.assert_called_with(id=checkout.id)

        self.assertTrue(test_data_missed.all_missed_check)
        self.assertFalse(test_data_nonmissed.all_missed_check)

    @patch("reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get")
    def test_test_data_all_missed_nonwaived_check(self, checkouts_get_mock):
        """Test the TestData's all_missed_nonwaived_check method."""
        from reporter.data import CheckoutData

        checkout = mocks.get_mocked_checkout(valid=True)
        build = mocks.get_mocked_build(checkout_id=checkout.id, valid=True)
        pass_test_waived = mocks.get_mocked_test(build_id=build.id, status="PASS", waived=True)
        miss_test_nonwaived = mocks.get_mocked_test(build_id=build.id, status="MISS", waived=False)
        miss_test_waived1 = mocks.get_mocked_test(build_id=build.id, status="MISS", waived=True)
        miss_test_waived2 = mocks.get_mocked_test(build_id=build.id, status="MISS", waived=True)

        kcidbfile_waived_pass = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=[build], tests=[miss_test_waived1, pass_test_waived]
        )
        kcidbfile_missed_nonwaived = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=[build], tests=[miss_test_nonwaived]
        )
        kcidbfile_missed_waived = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=[build], tests=[miss_test_waived2]
        )

        checkouts_get_mock.return_value.all.get.return_value = kcidbfile_waived_pass
        test_data_waived_pass = CheckoutData(checkout.id).test_data
        checkouts_get_mock.assert_called_with(id=checkout.id)

        checkouts_get_mock.return_value.all.get.return_value = kcidbfile_missed_nonwaived
        test_data_missed_nonwaived = CheckoutData(checkout.id).test_data
        checkouts_get_mock.assert_called_with(id=checkout.id)

        checkouts_get_mock.return_value.all.get.return_value = kcidbfile_missed_waived
        test_data_missed_waived = CheckoutData(checkout.id).test_data
        checkouts_get_mock.assert_called_with(id=checkout.id)

        self.assertFalse(test_data_waived_pass.all_missed_check)
        self.assertTrue(test_data_waived_pass.all_missed_nonwaived_check)
        self.assertTrue(test_data_missed_nonwaived.all_missed_check)
        self.assertTrue(test_data_missed_nonwaived.all_missed_nonwaived_check)
        self.assertTrue(test_data_missed_waived.all_missed_check)
        self.assertTrue(test_data_missed_waived.all_missed_nonwaived_check)

    @patch.object(TestData, "untriaged_tests", new_callable=mock.PropertyMock)
    @patch("reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get")
    def test_test_data_unknown_issues_tests_deprecated(
        self, checkouts_get_mock, mocked_untriaged_tests
    ):
        """Ensure we're warning about deprecated property TestData.unknown_issues_tests."""
        mocked_untriaged_tests.return_value = mock.sentinel.untriaged_tests

        checkout = mocks.get_mocked_checkout(valid=True)
        build = mocks.get_mocked_build(checkout_id=checkout.id, valid=True)
        test = mocks.get_mocked_test(build_id=build.id, status="PASS", waived=True)
        checkouts_get_mock.return_value.all.get.return_value = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=[build], tests=[test]
        )

        test_data = CheckoutData("redhat:1").test_data
        checkouts_get_mock.assert_called_with(id="redhat:1")  # smoke test

        with self.assertWarns(DeprecationWarning):
            result = test_data.unknown_issues_tests

        self.assertEqual(
            result,
            mock.sentinel.untriaged_tests,
            "Expected 'unknown_issues_tests' to return the outcome from 'untriaged_tests'",
        )

    @patch.object(TestData, "triaged_tests", new_callable=mock.PropertyMock)
    @patch("reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get")
    def test_test_data_known_issues_tests_deprecated(
        self, checkouts_get_mock, mocked_triaged_tests
    ):
        """Ensure we're warning about deprecated property TestData.known_issues_tests."""
        mocked_triaged_tests.return_value = mock.sentinel.triaged_tests

        checkout = mocks.get_mocked_checkout(valid=True)
        build = mocks.get_mocked_build(checkout_id=checkout.id, valid=True)
        test = mocks.get_mocked_test(build_id=build.id, status="PASS", waived=True)
        checkouts_get_mock.return_value.all.get.return_value = mocks.get_mocked_kcidbfile(
            checkouts=[checkout], builds=[build], tests=[test]
        )

        test_data = CheckoutData("redhat:1").test_data
        checkouts_get_mock.assert_called_with(id="redhat:1")  # smoke test

        with self.assertWarns(DeprecationWarning):
            result = test_data.known_issues_tests

        self.assertEqual(
            result,
            mock.sentinel.triaged_tests,
            "Expected 'known_issues_tests' to return the outcome from 'triaged_tests'",
        )
