from argparse import Namespace
import contextlib
import errno
from importlib import resources
import io
import itertools
import json
import pathlib
import re
import tempfile
import textwrap
from unittest import TestCase
from unittest import mock
from unittest.mock import MagicMock
from unittest.mock import patch

from cki_lib.kcidb import KCIDBFile
import responses

from reporter import settings
from reporter.settings import LOGGER

from . import assets
from .utils import load_json

ASSETS = resources.files(assets)


class TestMain(TestCase):
    """Tests for reporter/__main__.py ."""

    input_file_path = ASSETS / "kcidb_all_checkout_127491_aarch64_reduced_and_triaged.json"
    input_file_r = pathlib.Path(input_file_path)
    default_kwargs = {
        "listen": False,
        "checkout_id": None,
        "build_id": None,
        "template": None,
        "email": False,
        "from_file": None,
        "input_file": None,
    }
    default_args = Namespace(**default_kwargs)
    listen_args = Namespace(**(default_kwargs | {"listen": True}))
    checkout_args = Namespace(**(default_kwargs | {"checkout_id": "redhat:123"}))
    build_filter_args = Namespace(
        **(default_kwargs | {"checkout_id": "redhat:123", "build_id": "2"})
    )
    template_args = Namespace(
        **(default_kwargs | {"checkout_id": "redhat:123", "template": "long", "input_file": None})
    )
    email_args = Namespace(**(default_kwargs | {"checkout_id": "redhat:123", "email": True}))
    input_file_args = Namespace(
        **(default_kwargs | {"checkout_id": "redhat:1163293372", "input_file": input_file_r})
    )
    from_file_args = Namespace(
        **(default_kwargs | {"checkout_id": "redhat:1163293372", "from_file": input_file_r})
    )

    def setUp(self):
        settings.EMAILING_ENABLED = False
        settings.DATAWAREHOUSE_URL = "http://localhost"
        settings.SELECTED_TEMPLATE = "short"

    @responses.activate
    @patch('reporter.data.CheckoutData')
    def test_process_message(self, data_mock):
        """Test the process_message function."""
        from reporter.__main__ import process_message

        processing_func_mock = MagicMock()

        expected_data = {
            'timestamp': '2020-07-28T14:51:09.845612+00:00',
            'status': 'ready_to_report',
            'object_type': 'checkout',
            'object': {'id': 'redhat:123'},
        }

        process_message(body=expected_data, func=processing_func_mock)

        self.assertEqual(processing_func_mock.call_count, 1)

        # Call with an unexpected object_type
        with patch.dict(expected_data, {'object_type': 'foo'}):
            process_message(body=expected_data, func=processing_func_mock)
        self.assertEqual(processing_func_mock.call_count, 1)

        # Call again with a status that should be ignored
        with patch.dict(expected_data, {'status': 'new'}):
            process_message(body=expected_data, func=processing_func_mock)
        self.assertEqual(processing_func_mock.call_count, 1)

        with patch.dict(expected_data, {'object': {'misc': {'retrigger': True}}}):
            process_message(body=expected_data, func=processing_func_mock)
        self.assertEqual(processing_func_mock.call_count, 1)

        with patch.dict(expected_data, {'object': {'misc': {'related_merge_request': True}}}):
            process_message(body=expected_data, func=processing_func_mock)
        self.assertEqual(processing_func_mock.call_count, 1)

        # test that none of the previous patches had any effect permanent effect
        process_message(body=expected_data, func=processing_func_mock)
        self.assertEqual(processing_func_mock.call_count, 2)

    def test_create_parser(self):
        """Test the create_parser function."""
        from reporter.__main__ import create_parser

        parser = create_parser()

        args = parser.parse_args([])
        self.assertEqual(args, self.default_args)

        args = parser.parse_args(['-l'])
        self.assertEqual(args, self.listen_args)

        args = parser.parse_args(['-c', self.checkout_args.checkout_id])
        self.assertEqual(args, self.checkout_args)

        with self.assertRaises(SystemExit) as exit_exception:
            parser.parse_args(['-l', '-c', self.checkout_args.checkout_id])
        self.assertEqual(exit_exception.exception.code, 2)

    @responses.activate
    @patch('cki_lib.misc.is_production')
    @patch('argparse.ArgumentParser.parse_args')
    @patch('cki_lib.messagequeue.MessageQueue.consume_messages')
    @patch.dict('os.environ', {'REPORTER_ROUTING_KEYS': 'key'})
    def test_main_default(self, consume_messages_mock, args_mock,
                          is_production_mock):
        """Test main with no arguments."""
        from reporter.__main__ import main

        args_mock.return_value = self.default_args
        is_production_mock.return_value = True
        main()
        assert consume_messages_mock.called

    @responses.activate
    @patch('argparse.ArgumentParser.parse_args')
    @patch('cki_lib.messagequeue.MessageQueue.consume_messages')
    @patch("cki_lib.metrics.prometheus_init")
    @patch.dict(
        "os.environ",
        {"WEBHOOK_RECEIVER_EXCHANGE": "x", "REPORTER_ROUTING_KEYS": "key", "REPORTER_QUEUE": "q"},
    )
    def test_main_listen(self, prom_mock, consume_messages_mock, args_mock):
        """Test main with listen argument."""
        from reporter.__main__ import main

        args_mock.return_value = self.listen_args
        main()
        prom_mock.assert_called_once_with()
        consume_messages_mock.assert_called_once_with("x", ["key"], mock.ANY, queue_name="q")

    @responses.activate
    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.data.CheckoutData')
    def test_main_checkout_id(self, checkout_data_mock, args_mock):
        """Test main with checkout id specified."""
        from reporter.__main__ import main

        args_mock.return_value = self.checkout_args
        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 0)
        checkout_data_mock.assert_called_once_with(
            self.checkout_args.checkout_id, self.checkout_args.input_file,
            self.checkout_args.build_id)

    @responses.activate
    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.data.CheckoutData')
    def test_main_build_id(self, checkout_data_mock, args_mock):
        """Test main with checkout id specified."""
        from reporter.__main__ import main

        args_mock.return_value = self.build_filter_args

        with self.assertRaises(SystemExit) as exit_exception:
            main()

        self.assertEqual(exit_exception.exception.code, 0)
        checkout_data_mock.assert_called_once_with(
            self.build_filter_args.checkout_id, self.build_filter_args.input_file,
            self.build_filter_args.build_id)

    @responses.activate
    @patch('reporter.emailer.send_emails', MagicMock())
    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.data.CheckoutData')
    def test_main_template(self, checkout_data_mock, args_mock):
        """Test main with template specified."""
        from reporter import settings
        from reporter.__main__ import main

        args_mock.return_value = self.template_args
        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 0)
        checkout_data_mock.assert_called_once_with(
            self.template_args.checkout_id,
            self.template_args.input_file,
            self.template_args.build_id
        )

        self.assertEqual(settings.SELECTED_TEMPLATE, self.template_args.template)

    @responses.activate
    @patch("cki_lib.kcidb.KCIDBFile", wraps=KCIDBFile)
    @patch('reporter.data.CheckoutData')
    @patch('argparse.ArgumentParser.parse_args')
    def test_main_input_file(self, args_mock, checkout_data_mock, kcidb_file_mock):
        """Test main, with input_file specified"""
        from reporter.__main__ import main

        args_mock.return_value = self.input_file_args

        with self.subTest("File is passed when it exists on disk"):
            with self.assertRaises(SystemExit) as exit_exception:
                main()
            self.assertEqual(exit_exception.exception.code, 0)
            checkout_data_mock.assert_called_once_with(
                self.input_file_args.checkout_id,
                mock.ANY,  # NOTE: KCIDBFile(self.input_file_args.input_file)
                self.input_file_args.build_id,
            )
            kcidb_file_mock.assert_called_once_with(self.input_file_args.input_file, validate=False)

        checkout_data_mock.reset_mock()
        kcidb_file_mock.reset_mock()

        with self.subTest("Exit when input_file can't be found"):
            self.input_file_args.input_file = pathlib.Path('something-nonexistent-in-this-dir.json')
            with (
                self.assertRaises(SystemExit) as exit_exception,
                self.assertLogs(logger=LOGGER, level="CRITICAL") as log_ctx,
            ):
                main()
            self.assertEqual(exit_exception.exception.code, errno.ENOENT)
            self.assertIn(
                (
                    f"CRITICAL:{LOGGER.name}:"
                    "Input file something-nonexistent-in-this-dir.json does not exist."
                ),
                log_ctx.output,
            )

            kcidb_file_mock.assert_not_called()
            checkout_data_mock.assert_not_called()

        checkout_data_mock.reset_mock()
        kcidb_file_mock.reset_mock()

        # Cover KCIDBFile with multiple checkouts
        with tempfile.NamedTemporaryFile() as tmp_filename:
            kcidb_data = {
                **KCIDBFile.DEFAULT_CONTENT,
                "checkouts": [
                    {"id": "redhat:1", "origin": "redhat"},
                    {"id": "redhat:2", "origin": "redhat"},
                ],
            }
            kcidb_file = pathlib.Path(tmp_filename.name)
            kcidb_file.write_text(json.dumps(kcidb_data), encoding="utf-8")
            self.input_file_args.input_file = kcidb_file

            with self.subTest("Exit when from_file has multiple checkouts, but no checkout_id"):
                self.input_file_args.checkout_id = None
                with (
                    self.assertRaises(SystemExit) as exit_exception,
                    self.assertLogs(logger=LOGGER, level="CRITICAL") as log_ctx,
                ):
                    main()

                self.assertEqual(exit_exception.exception.code, errno.EINVAL)
                self.assertIn(
                    (
                        f"CRITICAL:{LOGGER.name}:"
                        "--checkout-id is required when --from-file FILE has multiple checkouts"
                    ),
                    log_ctx.output,
                )
                kcidb_file_mock.assert_called_once_with(kcidb_file, validate=False)
                checkout_data_mock.assert_not_called()

            checkout_data_mock.reset_mock()
            kcidb_file_mock.reset_mock()

            with self.subTest("Success when from_file has multiple checkouts and checkout_id"):
                self.input_file_args.checkout_id = "redhat:1"
                with (
                    self.assertRaises(SystemExit) as exit_exception,
                    self.assertLogs(logger=LOGGER, level="CRITICAL") as log_ctx,
                ):
                    main()

                self.assertEqual(exit_exception.exception.code, 0)
                checkout_data_mock.assert_called_once_with(
                    self.input_file_args.checkout_id,
                    mock.ANY,  # NOTE: KCIDBFile(kcidb_file)
                    self.input_file_args.build_id,
                )
                kcidb_file_mock.assert_called_once_with(kcidb_file, validate=False)

            checkout_data_mock.reset_mock()
            kcidb_file_mock.reset_mock()

    @responses.activate
    @patch("cki_lib.kcidb.KCIDBFile", wraps=KCIDBFile)
    @patch("reporter.data.CheckoutData")
    @patch("argparse.ArgumentParser.parse_args")
    def test_main_from_file(self, args_mock, checkout_data_mock, kcidb_file_mock):
        """Test main, with from_file specified"""
        from reporter.__main__ import main

        args_mock.return_value = self.from_file_args

        with self.subTest("File is passed when it exists on disk"):
            with self.assertRaises(SystemExit) as exit_exception:
                main()
            self.assertEqual(exit_exception.exception.code, 0)
            checkout_data_mock.assert_called_once_with(
                self.from_file_args.checkout_id,
                mock.ANY,  # NOTE: KCIDBFile(self.from_file_args.from_file)
                self.from_file_args.build_id,
            )
            kcidb_file_mock.assert_called_once_with(self.from_file_args.from_file, validate=False)

        checkout_data_mock.reset_mock()
        kcidb_file_mock.reset_mock()

        with self.subTest("Exit when from_file can't be found"):
            self.from_file_args.from_file = pathlib.Path("something-nonexistent-in-this-dir.json")
            with (
                self.assertRaises(SystemExit) as exit_exception,
                self.assertLogs(logger=LOGGER, level="CRITICAL") as log_ctx,
            ):
                main()
            self.assertEqual(exit_exception.exception.code, errno.ENOENT)
            self.assertIn(
                (
                    f"CRITICAL:{LOGGER.name}:"
                    "Input file something-nonexistent-in-this-dir.json does not exist."
                ),
                log_ctx.output,
            )

            kcidb_file_mock.assert_not_called()
            checkout_data_mock.assert_not_called()

        checkout_data_mock.reset_mock()
        kcidb_file_mock.reset_mock()

        # Cover KCIDBFile with multiple checkouts
        with tempfile.NamedTemporaryFile() as tmp_filename:
            kcidb_data = {
                **KCIDBFile.DEFAULT_CONTENT,
                "checkouts": [
                    {"id": "redhat:1", "origin": "redhat"},
                    {"id": "redhat:2", "origin": "redhat"},
                ],
            }
            kcidb_file = pathlib.Path(tmp_filename.name)
            kcidb_file.write_text(json.dumps(kcidb_data), encoding="utf-8")
            self.from_file_args.from_file = kcidb_file

            with self.subTest("Exit when from_file has multiple checkouts, but no checkout_id"):
                self.from_file_args.checkout_id = None
                with (
                    self.assertRaises(SystemExit) as exit_exception,
                    self.assertLogs(logger=LOGGER, level="CRITICAL") as log_ctx,
                ):
                    main()

                self.assertEqual(exit_exception.exception.code, errno.EINVAL)
                self.assertIn(
                    (
                        f"CRITICAL:{LOGGER.name}:"
                        "--checkout-id is required when --from-file FILE has multiple checkouts"
                    ),
                    log_ctx.output,
                )
                kcidb_file_mock.assert_called_once_with(kcidb_file, validate=False)
                checkout_data_mock.assert_not_called()

            checkout_data_mock.reset_mock()
            kcidb_file_mock.reset_mock()

            with self.subTest("Success when from_file has multiple checkouts and checkout_id"):
                self.from_file_args.checkout_id = "redhat:1"
                with (
                    self.assertRaises(SystemExit) as exit_exception,
                    self.assertLogs(logger=LOGGER, level="CRITICAL") as log_ctx,
                ):
                    main()

                self.assertEqual(exit_exception.exception.code, 0)
                checkout_data_mock.assert_called_once_with(
                    self.from_file_args.checkout_id,
                    mock.ANY,  # NOTE: KCIDBFile(kcidb_file)
                    self.from_file_args.build_id,
                )
                kcidb_file_mock.assert_called_once_with(kcidb_file, validate=False)

            checkout_data_mock.reset_mock()
            kcidb_file_mock.reset_mock()

    @responses.activate
    @patch('reporter.emailer.send_emails', MagicMock())
    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.data.CheckoutData')
    def test_main_exit(self, checkout_data_mock, args_mock):
        """Test main with checkout id specified."""
        from reporter.__main__ import main

        args_mock.return_value = self.template_args

        checkout_data_mock.return_value.result = True
        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 0)

        checkout_data_mock.return_value.result = False
        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 1)

    @responses.activate
    @patch('reporter.emailer.send_emails', MagicMock())
    @patch('argparse.ArgumentParser.parse_args')
    def test_main_exit_with_subtests_failures(self, args_mock):
        """Test that non-passing subtests still exit as a success if the test was waived."""
        from reporter.__main__ import main
        checkout = {'id': 'redhat:884677988', 'misc': {'iid': 89105}}
        responses.get(
            f"http://localhost/api/1/kcidb/checkouts/{checkout['id']}",
            json=checkout,
        )
        # NOTE: mock both endpoints until datawarehouse-api-lib!45 is merged
        responses.get(
            f"http://localhost/api/1/kcidb/checkouts/{checkout['misc']['iid']}/all",
            json=load_json("checkout_all_with_waived_test_and_failed_subtest.json"),
        )
        responses.get(
            f"http://localhost/api/1/kcidb/checkouts/{checkout['id']}/all",
            json=load_json("checkout_all_with_waived_test_and_failed_subtest.json"),
        )

        args_mock.return_value = Namespace(
            **(self.default_kwargs | {"checkout_id": checkout["id"], "template": "failure-only"})
        )

        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 0)

    @responses.activate
    @patch('argparse.ArgumentParser.parse_args')
    @patch('builtins.print')
    @patch('reporter.emailer.render_template')
    @patch('reporter.data.CheckoutData')
    def test_main_print(self, checkout_data_mock, render_template_mock, print_mock, args_mock):
        """Test main with listen argument."""
        from reporter.__main__ import main

        args_mock.return_value = self.checkout_args
        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 0)
        checkout_data_mock.assert_called_once_with(self.checkout_args.checkout_id,
                                                   self.checkout_args.input_file,
                                                   self.checkout_args.build_id)
        render_template_mock.assert_called_once_with(checkout_data_mock())
        print_mock.assert_called_once_with(render_template_mock())

    @responses.activate
    @patch('reporter.emailer.get_subject', MagicMock(return_value="subject"))
    @patch('smtplib.SMTP', MagicMock())
    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.emailer.send_emails')
    @patch('reporter.data.CheckoutData')
    def test_main_send_emails(self, checkout_data_mock, send_emails_mock, args_mock):
        """Test main with listen argument."""
        from reporter.__main__ import main

        args_mock.return_value = self.email_args
        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 0)
        checkout_data_mock.assert_called_once_with(self.email_args.checkout_id,
                                                   self.email_args.input_file,
                                                   self.email_args.build_id)
        send_emails_mock.assert_called_once_with(checkout_data_mock())

    @responses.activate
    @patch('reporter.data.CheckoutData')
    def test_render_template_shows_status_and_emojis(self, mock):
        """Test that the report shows the test statuses and their emojis."""
        from reporter.settings import JINJA_ENV
        test_data = load_json('test_data.json')
        mock.test_data.configure_mock(**test_data)
        template = JINJA_ENV.get_template('partials/all_tests.j2')
        text = template.render({'test_data': mock.test_data}).strip()
        # Keep only the part of the template that we're testing.
        m = re.match(r'(.*)Aborted tests', text, re.DOTALL)
        text = m.group(1)
        self.assertIn('✅ PASS LTP - syscalls', text)
        self.assertIn('❌ FAIL LTP - syscalls', text)
        self.assertIn('⚡ 🚧 ERROR kdump - sysrq-c', text)

    @responses.activate
    @patch('reporter.data.CheckoutData')
    def test_render_template_shows_subtests(self, mock):
        """Test that the report shows the non-passing subtests."""
        from reporter.settings import JINJA_ENV
        test_data = load_json('test_data.json')
        mock.test_data.configure_mock(**test_data)
        template = JINJA_ENV.get_template('partials/all_tests.j2')

        text = template.render({'test_data': mock.test_data}).strip()

        self.assertIn('❌ FAIL subtest1', text)
        self.assertIn('Subtests missed: 1 out of 3', text)
        self.assertNotIn('subtest2', text)
        self.assertNotIn('subtest3', text)
        self.assertNotIn('subtest4', text)

    @responses.activate
    @patch('reporter.data.CheckoutData')
    def test_render_template_build_debug(self, mock):
        """Test that the report works for all build debug values and exceptions."""
        from reporter.settings import JINJA_ENV
        build_data = load_json('build_data.json')
        mock.build_data.configure_mock(**build_data)
        template = JINJA_ENV.get_template('partials/all_builds.j2')
        text = template.render({'build_data': mock.build_data}).strip()

        self.assertIn('x86_64 (debug)', text)
        self.assertIn('ppc64', text)
        self.assertNotIn('ppc64 (debug)', text)
        self.assertIn('aarch64', text)
        self.assertNotIn('aarch64 (debug)', text)
        self.assertIn('aarch64 (64k)', text)
        self.assertIn('aarch64 (64k-debug)', text)
        self.assertNotIn('aarch64 ()', text)

    @responses.activate
    @patch("argparse.ArgumentParser.parse_args")
    def test_template_snapshots(self, args_mock):
        """Tests reporter outputs what is expected based on given input files and templates."""
        from reporter import settings
        from reporter.__main__ import main

        domain = {
            "cases": [
                "failed_checkout",
                "partially_triaged_build",
                "partially_triaged_test",
                "fully_triaged_test",
                "regression_test",
            ],
            "templates": settings.TEMPLATES,
        }

        # Mock for (case='failed_checkout', template='long')
        responses.get(
            (
                "https://s3.amazonaws.com/arr-cki-prod-trusted-artifacts/trusted-artifacts/"
                "1566751218/merge/8508920191/artifacts/merge.log"
            ),
            body=textwrap.dedent(
                """
                Auto-merging Makefile.rhelver
                Auto-merging include/linux/local_lock.h
                CONFLICT (content): Merge conflict in include/linux/local_lock.h
                Auto-merging include/linux/local_lock_internal.h
                CONFLICT (content): Merge conflict in include/linux/local_lock_internal.h
                Automatic merge failed; fix conflicts and then commit the result.
                """
            ).strip(),
        )

        for case, template in itertools.product(*domain.values()):
            with self.subTest(case=case, template=template):
                input_path = ASSETS / f"input/kcidb_all-{case}.json"
                expected_output_path = ASSETS / f"output/{template}-{case}.txt"

                # Prepare input
                args_mock.return_value = Namespace(
                    **(self.default_kwargs | {"template": template, "from_file": input_path})
                )

                # Compute output
                stdout = io.StringIO()
                with contextlib.redirect_stdout(stdout):
                    with contextlib.suppress(SystemExit):
                        main()
                    result = stdout.getvalue()

                # Validate against expected, creating if it doesn't exist
                if not expected_output_path.exists():
                    with expected_output_path.open("w") as result_file:
                        result_file.write(result)
                    raise AssertionError(f"Created {expected_output_path}. Check and commit!")
                expected = expected_output_path.read_text()
                self.assertEqual(result, expected)
