Hi, we tested your kernel and here are the results:

    Overall result: FAILED
             Merge: FAILED


Kernel information:
    Merge Request: https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9/-/merge_requests/5892

You can find all the details about the test run at
    http://localhost/kcidb/checkouts/redhat:1566751218

Merge log:
    Auto-merging Makefile.rhelver
    Auto-merging include/linux/local_lock.h
    CONFLICT (content): Merge conflict in include/linux/local_lock.h
    Auto-merging include/linux/local_lock_internal.h
    CONFLICT (content): Merge conflict in include/linux/local_lock_internal.h
    Automatic merge failed; fix conflicts and then commit the result.


If you find a failure unrelated to your changes, please ask the test maintainer to review it.
This will prevent the failures from being incorrectly reported in the future.

All user documentation, including reproducing the run: https://cki-project.org/l/user-docs

In case of any CKI bugs or questions, reach out to <cki-project@redhat.com> .
