Hi, we tested your kernel and here are the failures:

 aarch64 - load/unload kernel module test - vm test:
    Issue: load/unload kernel module test: builtin modules causes test to fail on RHEL9
           https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/578
    Logs: http://localhost/kcidb/tests/redhat:497294389_aarch64_upt_29

    Note: Any of these might not be a regression and/or we might have mistakenly thought this issue to be fixed.
          If it is not a regression, please ask the test maintainer to reopen the issue and untag the regression from this execution.


You can find all the details about the test run at
    http://localhost/kcidb/checkouts/redhat:497294389

If you find a failure unrelated to your changes, please ask the test maintainer to review it.
This will prevent the failures from being incorrectly reported in the future.

All user documentation, including reproducing the run: https://cki-project.org/l/user-docs

In case of any CKI bugs or questions, reach out to <cki-project@redhat.com> .
