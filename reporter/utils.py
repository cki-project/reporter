"""Utility functions like custom jinja filters."""
import itertools
import json
import re

from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
from datawarehouse.objects import KCIDBCheckout
from datawarehouse.objects import KCIDBTest
from datawarehouse.objects import KCIDBTestResult

LOGGER = get_logger("cki.reporter")


def add_cross_references(all_data, *, build_id: str = ""):  # noqa: C901, PLR0912, PLR0915
    # pylint: disable=too-many-branches,too-many-statements
    """Given checkout-all data, add cross references between objects, dropping any orphans."""
    _checkouts = {}
    _builds = {}
    _tests = {}
    _testresults = {}
    _issueoccurrences = []

    for checkout in all_data.checkouts:
        _checkouts[checkout.id] = checkout
        checkout.builds = []
        checkout.issues_occurrences = []

    for build in all_data.builds:
        if not (checkout := _checkouts.get(build.checkout_id)):
            LOGGER.warning("Missing parent checkout %r in %r", build.checkout_id, build.id)
        else:
            checkout.builds.append(build)
            build.checkout = checkout
            _builds[build.id] = build
            build.tests = []
            build.issues_occurrences = []

    for test in all_data.tests:
        if not (build := _builds.get(test.build_id)):
            LOGGER.warning("Missing parent build %r in %r", test.build_id, test.id)
        else:
            build.tests.append(test)
            test.build = build
            test.architecture = build.architecture
            # FIXME: `variant` is a BuildData field  # pylint: disable=fixme
            test.variant = ""
            test.debug = build.misc.get("debug", False)
            _tests[test.id] = test
            test.testresults = []
            test.issues_occurrences = []
            test.waived_failure = test.status == "FAIL" and test.waived

    for testresult in all_data.testresults:
        if not (test := _tests.get(testresult.test_id)):
            LOGGER.warning("Missing parent test %r in %r", testresult.test_id, testresult.id)
        else:
            test.testresults.append(testresult)
            testresult.test = test
            _testresults[testresult.id] = testresult
            testresult.issues_occurrences = []

    for occurrence in all_data.issueoccurrences:
        # NOTE: checkout must be validated last because it's always filled by "dw-api-lib"
        if occurrence.testresult_id:
            kcidb_object = _testresults.get(occurrence.testresult_id)
        elif occurrence.test_id:
            kcidb_object = _tests.get(occurrence.test_id)
        elif occurrence.build_id:
            kcidb_object = _builds.get(occurrence.build_id)
        else:
            kcidb_object = _checkouts.get(occurrence.checkout_id)

        if not kcidb_object:
            LOGGER.warning("Missing KCIDB object in %r", occurrence)
        else:
            kcidb_object.issues_occurrences.append(occurrence)
            _issueoccurrences.append(occurrence)

    # Update all_data without the orphaned objects, filtering by build_id if present
    if not build_id:
        all_data.checkouts = list(_checkouts.values())
        all_data.builds = list(_builds.values())
        all_data.tests = list(_tests.values())
        all_data.testresults = list(_testresults.values())
        all_data.issueoccurrences = _issueoccurrences
    elif build := _builds.get(build_id):
        all_data.checkouts = [build.checkout]
        all_data.builds = [build]
        all_data.tests = build.tests
        all_data.testresults = list(
            itertools.chain.from_iterable(test.testresults for test in all_data.tests)
        )
        all_data.issueoccurrences = list(
            itertools.chain.from_iterable(
                kcidb_object.issues_occurrences
                for kcidb_object in itertools.chain(
                    all_data.checkouts,
                    all_data.builds,
                    all_data.tests,
                    all_data.testresults,
                )
            )
        )
    else:
        LOGGER.warning("No build matching the given ID: %s", build_id)
        all_data.checkouts = []
        all_data.builds = []
        all_data.tests = []
        all_data.testresults = []
        all_data.issueoccurrences = []

    # Append occurrence to parent test mainly to show in case of regressions
    for test in all_data.tests:
        test.issues_occurrences.extend(
            itertools.chain.from_iterable(
                testresult.issues_occurrences for testresult in test.testresults
            )
        )


def add_testresults_to_kcidbfile_dict(kcidb_file):
    """Iterate over tests, extracting results to kcidb_file.data['testresults'].

    Testresults are then used then by TestData class.
    """
    overall_testresults = []
    for test in kcidb_file.data.get('tests', []):
        individual_testresults = get_nested_key(test, 'misc/results', [])
        for result in individual_testresults:
            # Add result["test_id"] to the kcidb_all file, this is needed for triaging the kcidb_all
            # file from the pipeline.
            result["test_id"] = test["id"]
        overall_testresults.extend(individual_testresults)
    kcidb_file.data['testresults'] = overall_testresults


def status_to_emoji(status):
    """Convert test status to emoji."""
    # https://github.com/kernelci/kcidb-io/blob/main/kcidb_io/schema/v4.py#L508
    return {
        'PASS': '✅',
        'FAIL': '❌',
        'ERROR': '⚡',
        'MISS': '🕳️',
        # Extra cases for the email subject
        'MISSED': '🟩',
        'SKIPPED': '⏭️',
    }.get(status, '❔')


def yesno(condition, values="yes,no"):
    """Return first value if True, second if False, third if None."""
    # Mimics the django templates builtin filter
    # https://docs.djangoproject.com/en/3.0/ref/templates/builtins/#yesno
    values = values.split(',')
    if condition:
        return values[0]

    if condition is None and len(values) > 2:
        return values[2]

    return values[1]


def unique_test_descriptions(tests):
    """Return only unique combinations of a test architecture, debug status and comment."""
    unique_tests = {}
    for test in tests:
        unique_attributes = (test.architecture, test.debug, test.comment)
        if unique_attributes not in unique_tests:
            unique_tests[unique_attributes] = test

    unique_tests = dict(sorted(unique_tests.items()))
    return list(unique_tests.values())


def join_addresses(addresses):
    """Join the addresses in a list together while stripping each."""
    return ', '.join(address.strip() for address in addresses)


def get_architecture_plus_variant(test):
    """Return a string containing the architecture and (if available) variant in parenthesis."""
    variant = f" ({test.variant})" if test.variant else ""
    return test.architecture + variant


def get_attributes(kcidb_data):
    """Walk through data serializing all nested `attributes`."""
    # NOTE: This function be replaced by a proper JSONEncoder in dw-api-lib or generic-rest-client
    try:
        match kcidb_data:
            case dict():
                serialized_data = {k: get_attributes(v) for k, v in kcidb_data.items()}
            case list():
                serialized_data = [get_attributes(item) for item in kcidb_data]
            case _:  # Any instance of restclient.base.RESTObject
                serialized_data = kcidb_data.attributes
    except AttributeError:
        msg = f"Failed to get attributes from {kcidb_data}"
        LOGGER.exception(msg)
        return {"error": msg}

    # Make sure KCIDBTestResults are serialized within tests
    if isinstance(kcidb_data, KCIDBTest) and (results := kcidb_data.testresults):
        serialized_data["misc"]["results"] = get_attributes(results)

    # remove undesired parent_attrs inherited from dw-api-lib
    if isinstance(kcidb_data, (KCIDBCheckout, KCIDBTest, KCIDBTestResult)):
        serialized_data.pop("checkout_id", None)

    return serialized_data


def to_json(data):
    """Serialize data as JSON."""
    return json.dumps(data, indent=2)


def get_artifacts(artifacts):
    """Try to get artifacts otherwise fallback to the raw artifacts list."""
    for artifact in artifacts:
        if match := re.search(r"(?P<prefix>.*)/artifacts", artifact["url"]):
            return [{"name": "index.html", "url": match.group("prefix") + "/index.html"}]

    return artifacts
