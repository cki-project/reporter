"""Data format to be passed to the template."""
from warnings import warn

from cached_property import cached_property
from cki_lib import kcidb
from cki_lib.kcidb.checks import broken_boot_tests
from cki_lib.session import get_session
from datawarehouse.objects import KCIDBFile

from . import settings
from . import utils

SESSION = get_session(__name__, raise_for_status=True)
TRIAGEABLE_STATUSES = {"FAIL", "ERROR", "MISS"}


class TestData:
    """Data about all checkout's tests."""

    __test__ = False  # inform pytest that this class should not be collected

    def __init__(self, all_data):
        """Initialize."""
        self.tests = all_data.tests

        # Filtering subtests so we don't have to do it in jinja
        for test in self.tests:
            test.missed_testresults = [
                testresult for testresult in test.testresults if testresult.status == "MISS"
            ]
            test.nonpassing_ran_testresults = [
                testresult
                for testresult in test.testresults
                if testresult.status in {"FAIL", "ERROR"}
            ]
            test.untriaged_testresults = [
                testresult
                for testresult in test.nonpassing_ran_testresults
                if not testresult.issues_occurrences
            ]

    def _tests_filtered(self, condition, exclude_waived=False):
        tests = self.tests
        if exclude_waived:
            tests = filter(lambda x: not x.waived, tests)
        return list(filter(condition, tests))

    @cached_property
    def nonmissed_tests(self):
        """Get a list of build's non-missed tests."""
        return self._tests_filtered(lambda test:
                                    test not in self.missed_tests)

    @cached_property
    def nonmissed_nonwaived_tests(self):
        """Get a list of build's non-missed and non-waived tests."""
        return self._tests_filtered(lambda test:
                                    test not in self.missed_tests,
                                    exclude_waived=True)

    @cached_property
    def passed_tests(self):
        """Get a list of build's tests that passed."""
        return self._tests_filtered(lambda test: test.status == 'PASS')

    @cached_property
    def missed_tests(self):
        """Get a list of build's tests that got missed."""
        return self._tests_filtered(lambda test: test.status is None or
                                    test.status == 'MISS')

    @cached_property
    def failed_tests(self):
        """Get a list of build's tests that failed."""
        return self._tests_filtered(lambda test: test.status == 'FAIL',
                                    exclude_waived=True)

    @cached_property
    def errored_tests(self):
        """Get a list of build's tests that encountered an infra error."""
        return self._tests_filtered(lambda test: test.status == 'ERROR')

    @cached_property
    def waived_tests(self):
        """Get a list of waived build's tests."""
        return self._tests_filtered(lambda test: test.waived)

    @cached_property
    def triaged_tests(self):
        """Get a list of build's tests that failed due to known issues."""
        return self._tests_filtered(
            lambda test: test.issues_occurrences
            and test not in self.regressions
            and (
                # If the test has results, all unsuccessful subtests must be triaged,
                # otherwise, the test being triaged is enough
                not test.testresults or not test.untriaged_testresults
            )
        )

    @cached_property
    def known_issues_tests(self):
        """Deprecated, use `triaged_tests` instead."""
        warn(
            (
                "Function `reporter.data.TestData.known_issues_tests` will be deprecated soon,"
                " please use `reporter.data.TestData.triaged_tests`."
            ),
            DeprecationWarning,
            stacklevel=2,
        )
        return self.triaged_tests

    @cached_property
    def untriaged_tests(self):
        """Get a list of build's tests that failed due to unknown issues."""
        return self._tests_filtered(
            lambda test: test.status in TRIAGEABLE_STATUSES
            and test not in self.triaged_tests
            and test not in self.waived_tests
            and test not in self.regressions
        )

    @cached_property
    def unknown_issues_tests(self):
        """Deprecated, use `untriaged_tests` instead."""
        warn(
            (
                "Function `reporter.data.TestData.unknown_issues_tests` will be deprecated soon,"
                " please use `reporter.data.TestData.untriaged_tests`."
            ),
            DeprecationWarning,
            stacklevel=2,
        )
        return self.untriaged_tests

    @cached_property
    def untriaged_failed_tests(self):
        """Get a list of build's tests that failed due to unknown issues."""
        return self._tests_filtered(
            lambda test: test.status == "FAIL"
            and test in self.untriaged_tests
            # Make sure there's at least one untriaged failed TestResult
            and (
                # If the test has results, it needs at least one untriaged failure,
                # otherwise, the test being untriaged is enough
                not test.testresults
                or any(tr.status == "FAIL" for tr in test.untriaged_testresults)
            )
        )

    @cached_property
    def regressions(self):
        """Get a list of tests that have resolved issues."""
        return self._tests_filtered(
            lambda test: any(occurrence.is_regression for occurrence in
                             test.issues_occurrences),
            exclude_waived=True
        )

    @cached_property
    def known_issues(self):
        """Get a list of all issues and the tests they affect."""
        issues = {}  # {"issue_ticket_url": {"issue": issue, "tests": [test1, test2, ...], ...}}
        for test in self.triaged_tests:
            for occurrence in test.issues_occurrences or []:
                if occurrence.issue['ticket_url'] not in issues:
                    issues[occurrence.issue['ticket_url']] = {'issue': occurrence.issue,
                                                              'tests': []}
                issues[occurrence.issue['ticket_url']]['tests'].append(test)
        return issues

    @cached_property
    def all_missed_check(self):
        """
        Check whether all tests were missed.

        True if every test was missed, False otherwise.
        """
        return not self.nonmissed_tests

    @cached_property
    def all_missed_nonwaived_check(self):
        """
        Check whether all non-waived tests were missed.

        True if every non-waived test was missed, False otherwise.
        """
        return not self.nonmissed_nonwaived_tests

    @cached_property
    def summary(self):
        """
        Get the test_data summary.

        None if all non-waived tests were missed, False if unknown failures found, True otherwise.
        """
        if self.tests and self.all_missed_nonwaived_check:
            return None
        return not (self.untriaged_failed_tests or self.regressions)


class BuildData:
    """Data about all checkout's builds."""

    def __init__(self, all_data, source_package_name='kernel'):
        """Initialize."""
        self.builds = all_data.builds
        for build in self.builds:
            # Rename in case this starts conflicting with an attribute
            build.variant = self.get_kernel_variant(build, source_package_name)

    @cached_property
    def passed_builds(self):
        """Return passing builds."""
        return list(filter(lambda x: x.valid is True, self.builds))

    @cached_property
    def failed_builds(self):
        """Return failing builds."""
        return list(filter(lambda x: x.valid is False, self.builds))

    @cached_property
    def known_issues_builds(self):
        """Get a list of builds that failed due to known issues."""
        return list(filter(lambda x: x.issues_occurrences, self.builds))

    @cached_property
    def unknown_issues_builds(self):
        """Get a list of builds that failed due to unknown issues."""
        return list(filter(lambda x: not x.issues_occurrences, self.failed_builds))

    @cached_property
    def non_booting_builds(self):
        """Return non-booting builds."""
        return list(filter(lambda x: x.boots is False, self.builds))

    @staticmethod
    def get_kernel_variant(build, source_package_name='kernel'):
        """Return builds "variant" to include with its architecture."""
        modifiers = []
        package_name = build.misc.get('package_name', 'kernel')
        debug = build.misc.get('debug', False)
        if package_name != source_package_name:
            modifiers.append(package_name.removeprefix(source_package_name).removeprefix('-'))
        if debug and 'debug' not in package_name:
            modifiers.append('debug')
        return '-'.join(modifiers)


class CheckoutData:
    """Data about a single checkout."""

    def __init__(self, checkout_id, kcidb_file=None, build_id=None):
        """Initialize checkout and builds either fetching the API or extracting it from a file."""
        if checkout_id is None:
            raise TypeError("checkout_id must be set")
        if checkout_id == '':
            raise ValueError("checkout_id can't be empty")

        if not kcidb_file:
            # Reporting using the API (online)
            checkouts = settings.DATAWAREHOUSE.kcidb.checkouts
            all_data = checkouts.get(id=checkout_id).all.get()
            self.checkout = all_data.checkouts[0]
        else:
            # NOTE: We want to move part of reporter's logic to cki-lib, and this is a start
            if not isinstance(kcidb_file, kcidb.KCIDBFile):
                warn(
                    (
                        "reporter.data.CheckoutData(checkout_id, kcidb_file, build_id) now expects"
                        f" a `cki_lib.kcidb.KCIDBFile` instead of `{type(kcidb_file).__name__}`."
                        " Please update your code calling "
                        f"`CheckoutData(..., kcidb_file=KCIDBFile(path, validate=False), ...)`."
                    ),
                    DeprecationWarning,
                    stacklevel=1,
                )
                kcidb_file = kcidb.KCIDBFile(kcidb_file, validate=False)

            # Make sure we have `testresults` in the top-level
            if "testresults" not in kcidb_file.data:
                utils.add_testresults_to_kcidbfile_dict(kcidb_file)
            else:
                settings.LOGGER.debug("File already has testresults in the top-level")

            dw = settings.DATAWAREHOUSE
            try:
                all_data = KCIDBFile(dw.kcidb.data, kcidb_file.data)
            except TypeError as error:
                raise ValueError("Failed to parse data into dw-api-lib instance") from error

            self.checkout = next(
                (checkout for checkout in all_data.checkouts if checkout.id == checkout_id),
                None
            )
            if self.checkout is None:
                raise ValueError(f"Checkout {checkout_id} not found in {kcidb_file}")

        utils.add_cross_references(all_data, build_id=build_id)

        self.all_data = all_data
        self.filter_non_booting_builds()

    @cached_property
    def build_data(self):
        """Get the BuildData object which contains information about builds."""
        return BuildData(self.all_data, self.checkout.misc.get("source_package_name", "kernel"))

    @cached_property
    def test_data(self):
        """Get the TestData object which contains information about tests."""
        return TestData(self.all_data)

    @cached_property
    def issues_occurrences(self):
        """Return the checkouts's known issues."""
        return self.checkout.issues_occurrences

    @cached_property
    def mergelog(self):
        """Return the checkout's mergelog file."""
        return SESSION.get(self.checkout.log_url).content.decode('utf8')

    def filter_non_booting_builds(self):
        """Check for non-booting builds and remove their tests as they don't provide any info."""
        if boot_tests := broken_boot_tests(dw_all=self.all_data):
            affected_build_ids = {test.build_id for test in boot_tests}
            settings.LOGGER.debug("Builds that failed to boot: %s", affected_build_ids)
            # Filter out non-boot-test tests of the affected builds
            self.all_data.tests = [test for test in self.all_data.tests
                                   if test.build_id not in affected_build_ids or test in boot_tests]
            for build in self.all_data.builds:
                build.boots = build.id not in affected_build_ids

    @cached_property
    def result(self):
        """
        Get the overall result.

        True if everything passed, False otherwise.
        """
        return (
            self.checkout.valid is not False  # Don't fail if valid is None
            and not self.build_data.unknown_issues_builds
            and not self.build_data.non_booting_builds
            and not self.test_data.untriaged_failed_tests
            and not self.test_data.regressions
        )

    @cached_property
    def summary(self):
        """
        Get the checkout summary, same as the result except when all non-waived tests were missed.

        None if all non-waived tests were missed, True if everything passed, False otherwise.
        """
        if self.result and self.test_data.summary is None:
            return None
        return self.result
